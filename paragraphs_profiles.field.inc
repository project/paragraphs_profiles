<?php
/**
 * @file
 * Field implementation for Paragraphs Profiles.
 */

/**
 * Implements hook_form_field_ui_field_edit_form_alter().
 */
function paragraphs_profiles_form_field_ui_field_edit_form_alter(&$form, &$form_state) {
  if (!empty($form['locked'])) {
    return;
  }

  // Determine if an item is disabled and just get the title.
  $not_disabled = function (&$item) {
    if (!empty($item->disabled)) {
      $item = isset($item->admin_title) ? $item->admin_title : $item->name;
      return TRUE;
    }

    return FALSE;
  };

  $profiles = paragraphs_profiles_get_profiles();
  // Filter out disabled profiles and change results to just contain name.
  $profiles = array_filter($profiles, $not_disabled);

  // Hard coded for now as I don't think there would be any other supported
  // field types in this case.
  if ($form['#field']['type'] === 'paragraphs') {
    $form['instance']['settings']['paragraphs_profiles'] = array(
      '#type' => 'fieldset',
      '#title' => t('Paragraphs Field Profile'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );

    $form['instance']['settings']['paragraphs_profiles']['selected_profile'] = array(
      '#type' => 'select',
      '#title' => t('Profile'),
      '#description' => t('Select which Paragraphs Profile should be applied to this field instance.') . '<br />' .
        t('Fields with existing paragraphs bundles will <strong>not</strong> have bundles added or removed. Other settings will apply however.'),
      '#options' => $profiles,
      '#empty_option' => t('- No Profile -'),
      '#default_value' => isset($form['#instance']['settings']['paragraphs_profiles']['selected_profile']) ? $form['#instance']['settings']['paragraphs_profiles']['selected_profile'] : '',
    );
  }
}
