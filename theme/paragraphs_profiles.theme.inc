<?php

/**
 * @file
 * Paragraphs Profiles theme functions.
 */

/**
 * Create admin settings draggable table with remove button.
 *
 * @param array $variables
 *   Variables for use in building the table.
 *
 * @return string
 *   Rendered HTML.
 */
function theme_paragraphs_profiles_form_table(&$variables) {
  $form = $variables['form'];
  $rows = array();
  $headers = array(
    t('Bundle Name'),
    t('Machine Name'),
    t('Weight'),
    t('Actions'),
  );

  foreach (element_children($form) as $id) {
    $form[$id]['weight']['#attributes']['class'] = array('weight');

    $fields = array(
      drupal_render($form[$id]['name']),
      drupal_render($form[$id]['machine_name']),
      drupal_render($form[$id]['weight']),
      drupal_render($form[$id]['remove_btn']),
    );

    $rows[$id]['data'] = $fields;
    $rows[$id]['class'] = array('draggable');
  }

  drupal_add_tabledrag('paragraphs-profiles-default-bundles', 'order', 'sibling', 'weight');

  return theme('table', array(
    'header' => $headers,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'paragraphs-profiles-default-bundles',
    ),
    'sticky' => FALSE,
    )
  );
}

/**
 * Render a multi-value paragraphs form with no table drag.
 *
 * @param array $variables
 *   Theme variables.
 *
 * @return string
 *   Rendered HTML.
 */
function theme_paragraphs_profiles_field_multiple_value_no_drag_form(&$variables) {
  $output = '';
  $element = $variables['form'];
  $instance = $element['#instance'];

  // Makes sense to keep these pointing to Paragraphs defaults.
  if (!isset($instance['settings']['title'])) {
    $instance['settings']['title'] = PARAGRAPHS_DEFAULT_TITLE;
  }

  if (!isset($instance['settings']['title_multiple'])) {
    $instance['settings']['title_multiple'] = PARAGRAPHS_DEFAULT_TITLE_MULTIPLE;
  }

  $add_mode = (isset($instance['settings']['add_mode']) ? $instance['settings']['add_mode'] : PARAGRAPHS_DEFAULT_ADD_MODE);

  $table_id = drupal_html_id($element['#field_name'] . '_values');
  $order_class = $element['#field_name'] . '-delta-order';
  $required = !empty($element['#required']) ? theme('form_required_marker', $variables) : '';

  $header = array(
    array(
      'data' => '<label>' . t('!title !required', array('!title' => $element['#title'], '!required' => $required)) . '</label>',
      'colspan' => 2,
      'class' => array('field-label'),
    ),
  );

  $rows = array();
  // Sort items according to '_weight' (needed when the form comes back after
  // preview or failed validation).
  $items = array();
  foreach (element_children($element) as $key) {
    if ($key === 'add_more') {
      $add_more_button = &$element[$key];
    }
    elseif ($key === 'add_more_type') {
      $add_more_button_type = &$element[$key];
    }
    elseif (!isset($element[$key]['#access']) || $element[$key]['#access']) {
      $items[] = &$element[$key];
    }
  }
  usort($items, '_field_sort_items_value_helper');

  // Add the items as table rows.
  foreach ($items as $key => $item) {
    $item['_weight']['#attributes']['class'] = array($order_class);
    $item['_weight']['#access'] = FALSE;
    $delta_element = drupal_render($item['_weight']);

    $cells = array(
      drupal_render($item),
    );

    $rows[] = array(
      'data' => $cells,
      'class' => array(
        drupal_html_class('paragraphs_item_type_' . $item['#bundle'])
      ),
    );
  }

  $output = '<div class="form-item">';
  if (count($items)) {
    $output .= theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(
        'id' => $table_id,
        'class' => array('field-multiple-table'),
        ),
    ));
  }
  else {
    $add_text = 'No !title_multiple added yet. Select a !title type and press the button below to add one.';
    if ($add_mode === 'button') {
      $add_text = 'No !title_multiple added yet. Select a !title type and press a button below to add one.';
    }
    $output .= '<label>' . t('!title !required', array('!title' => $element['#title'], '!required' => $required)) . '</label>';
    $output .= '<p><em>' . t($add_text, array('!title_multiple' => t($instance['settings']['title_multiple']), '!title' => t($instance['settings']['title']))) . '</em></p>';
  }
  $output .= $element['#description'] ? '<div class="description">' . $element['#description'] . '</div>' : '';
  $output .= '<div class="clearfix">' . drupal_render($add_more_button_type) . drupal_render($add_more_button) . '</div>';
  $output .= '</div>';

  return $output;
}
