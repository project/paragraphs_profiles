<?php

/**
 * @file
 * Plugin definition for ctools Export UI.
 */

$plugin = array(
  'schema' => 'paragraphs_profiles',
  'access' => 'administer paragraphs profiles',
  'menu' => array(
    'menu prefix' => 'admin/config/content',
    'menu item' => 'paragraphs-profiles',
    'menu title' => 'Paragraphs Profiles',
    'menu description' => t('Manage Paragraphs Profiles'),
  ),

  'title singular' => t('Paragraphs Profile'),
  'title plural' => t('Paragraphs Profiles'),
  'title singular proper' => t('Paragraphs Profile'),
  'title plural proper' => t('Paragraphs Profiles'),

  'form' => array(
    'settings' => 'paragraphs_profiles_ctools_export_ui_form',
  ),
);

/**
 * Paragraphs Profiles Admin Form.
 *
 * @param array $form
 *   An array of form elements to build the form.
 * @param array $form_state
 *   An array of form state information.
 */
function paragraphs_profiles_ctools_export_ui_form(&$form, &$form_state) {
  $profile = $form_state['item'];
  $bundles = paragraphs_bundle_load();

  // Get the name of an item.
  $get_item_name = function ($item) {
    return $item->name;
  };

  // Get an array of bundle names keyed by bundle machine name.
  $bundles_list = array_map($get_item_name, $bundles);

  $form['data'] = array(
    '#tree' => TRUE,
    '#type' => 'vertical_tabs',
  );

  $form['data']['profile_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic Settings'),
  );

  $form['data']['profile_settings']['restrict_add_more'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove option to add more paragraphs'),
    '#default_value' => isset($profile->data['profile_settings']['restrict_add_more']) ? $profile->data['profile_settings']['restrict_add_more'] : 0,
  );

  $form['data']['profile_settings']['restrict_remove'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove option to remove paragraphs'),
    '#default_value' => isset($profile->data['profile_settings']['restrict_remove']) ? $profile->data['profile_settings']['restrict_remove'] : 0,
  );

  $form['data']['profile_settings']['lock_default_bundle_order'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove option to re-order paragraph bundles'),
    '#default_value' => isset($profile->data['profile_settings']['lock_default_bundle_order']) ? $profile->data['profile_settings']['lock_default_bundle_order'] : 0,
  );

  $form['data']['profile_settings']['default_bundles'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="paragraph-profiles__default_bundles">',
    '#suffix' => '</div>',
    '#parents' => array('data', 'profile_settings', 'default_bundles'),
    '#theme' => 'paragraphs_profiles_form_table',
  );

  $tmp = array();

  $to_remove = FALSE;
  // Not ideal to be checking this based on the #value...
  if (!empty($form_state['triggering_element']) && $form_state['triggering_element']['#value'] === 'Remove') {
    $form_state['rebuild'] = TRUE;
    $to_remove = $form_state['triggering_element']['#id'];
  }

  // Populate the default bundles from previous settings.
  if (!empty($profile->data['profile_settings']['default_bundles'])) {
    foreach ($profile->data['profile_settings']['default_bundles'] as $key => $row) {
      // Don't re-add row if it's been deleted.
      if ($to_remove && $to_remove === $key) {
        unset($profile->data['profile_settings']['default_bundles'][$key]);
        continue;
      }

      $machine_name = check_plain($row['machine_name']['hidden']);
      $tmp[$key]['name'] = array('#markup' => $bundles[$machine_name]->name);

      // The machine name shouldn't really be an editable form item.
      // But the value needs to be stored for reference later,
      // so create a container to house a hidden field and a readable name.
      $tmp[$key]['machine_name'] = array(
        '#type' => 'container',
        '#title' => t('Machine Name'),
        '#title_display' => 'invisible',
      );

      $tmp[$key]['machine_name']['name'] = array(
        '#markup' => $machine_name,
      );

      $tmp[$key]['machine_name']['hidden'] = array(
        '#type' => 'hidden',
        '#value' => $machine_name,
      );

      $tmp[$key]['weight'] = array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => (int) $row['weight'],
      );

      $tmp[$key]['remove_btn'] = array(
        '#type' => 'button',
        '#name' => 'op_remove_' . $key,
        '#value' => t('Remove'),
        '#id' => $key,
        '#ajax' => array(
          'callback' => 'paragraph_profiles_remove_default_bundle',
          'wrapper' => 'paragraph-profiles__default_bundles',
        ),
      );
    }
  }

  uasort($tmp,
    function ($a, $b) {
      return $a['weight']['#default_value'] >= $b['weight']['#default_value'];
    }
  );

  if (!empty($form_state['triggering_element']) && $form_state['triggering_element']['#value'] === 'Add Bundle') {
    $machine_name = check_plain($form_state['values']['data']['profile_settings']['bundle_options']);
    // Generate a key.
    $key = $machine_name . '_' . count($tmp);
    $form_state['rebuild'] = TRUE;
    $tmp_items_count = count($tmp);

    $new_row['name'] = array('#markup' => $bundles[$machine_name]->name);

    // The machine name shouldn't really be an editable form item.
    // But the value needs to be stored for reference later,
    // so create a container to house a hidden field and a human-readable name.
    $new_row['machine_name'] = array(
      '#type' => 'container',
      '#title' => t('Machine Name'),
      '#title_display' => 'invisible',
    );
    $new_row['machine_name']['name'] = array(
      '#markup' => $machine_name,
    );
    $new_row['machine_name']['hidden'] = array(
      '#type' => 'hidden',
      '#value' => $machine_name,
    );

    $new_row['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#title_display' => 'invisible',
      '#default_value' => $tmp_items_count,
    );

    $new_row['remove_btn'] = array(
      '#type' => 'button',
      '#name' => 'op_remove_' . $tmp_items_count,
      '#id' => $key,
      '#value' => t('Remove'),
      '#ajax' => array(
        'callback' => 'paragraph_profiles_remove_default_bundle',
        'wrapper' => 'paragraph-profiles__default_bundles',
      ),
    );

    $tmp[$key] = $new_row;

    // Append to form state.
    $form_state_item = array(
      'machine_name' => array(
        'hidden' => $machine_name,
      ),
      'weight' => 0,
    );

    $profile->data['profile_settings']['default_bundles'][$key] = $form_state_item;
  }

  foreach ($tmp as $key => $element) {
    $form['data']['profile_settings']['default_bundles'][$key] = $element;
  }

  $form['data']['profile_settings']['bundle_selection_description'] = array(
    '#type' => 'item',
    '#description' => t('These settings will not override field cardinality. Paragraphs will be added in this order until max number of items is reached.') .
      '<br />' . t('"Allowed Bundles" settings on the field instance will be ignored when adding these defaults.'),
  );

  $form['data']['profile_settings']['bundle_options'] = array(
    '#type' => 'select',
    '#options' => $bundles_list,
  );

  $form['data']['profile_settings']['add_bundle'] = array(
    '#type' => 'button',
    '#name' => 'op_add',
    '#value' => t('Add Bundle'),
    '#ajax' => array(
      'callback' => 'paragraph_profiles_add_default_bundle',
      'wrapper' => 'paragraph-profiles__default_bundles',
    ),
  );
}

/**
 * Add a default bundle to admin settings via ajax.
 *
 * @param array $form
 *   An array containing form information.
 * @param array $form_state
 *   An array containing the current state of the form.
 *
 * @return mixed
 *   The default bundles section of the form with the updated values.
 */
function paragraph_profiles_add_default_bundle($form, $form_state) {
  return $form['data']['profile_settings']['default_bundles'];
}

/**
 * Remove a default bundle from the admin settings via ajax.
 *
 * @param array $form
 *   An array containing form information.
 * @param array $form_state
 *   An array containing the current state of the form.
 *
 * @return mixed
 *   The default bundles section of the form with the updated values.
 */
function paragraph_profiles_remove_default_bundle($form, $form_state) {
  return $form['data']['profile_settings']['default_bundles'];
}

/**
 * Paragraphs Profiles submit handler.
 *
 * @param array $form
 *   An array of form elements to build the form.
 * @param array $form_state
 *   An array of form state information.
 */
function paragraphs_profiles_ctools_export_ui_form_submit(&$form, &$form_state) {
  // Remove data we don't need to save.
  unset($form_state['values']['data']['profile_settings']['bundle_options']);
  unset($form_state['values']['data']['profile_settings']['add_bundle']);
  unset($form_state['values']['data']['data__active_tab']);
}
