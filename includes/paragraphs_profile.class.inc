<?php

/**
 * @file
 * Paragraphs Profile class.
 */

class ParagraphsProfile {
  public $data;

  /**
   * Magic getter function for settings in data.
   *
   * Streamlines accessing some of the properties through the data array.
   *
   * @param string $name
   *   Name of the setting to get.
   *
   * @return mixed
   *   Setting if found, or NULL otherwise.
   */
  public function __get($name) {
    $result =& drupal_static(__FUNCTION__);

    if (isset($result[$name])) {
      return $result[$name];
    }

    $result[$name] = NULL;

    if (isset($this->data['profile_settings'][$name])) {
      $result[$name] = $this->data['profile_settings'][$name];
    }
    elseif (isset($this->data['allowed_bundles'][$name])) {
      $result[$name] = $this->data['allowed_bundles'][$name];
    }

    return $result[$name];
  }

  /**
   * Get an array of default bundles, sorted by weight.
   *
   * @return array
   *   An array of bundles or an empty array.
   */
  public function getSortedDefaultBundles() {
    $bundles = $this->data['profile_settings']['default_bundles'];

    uasort($bundles,
      function ($a, $b) {
        return $a['weight'] >= $b['weight'];
      }
    );

    return ($bundles === FALSE) ? array() : $bundles;
  }
}
